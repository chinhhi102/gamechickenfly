﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEn_360 : MonoBehaviour
{
    public int nav = 1;
    public bool thow;
    public Animator animator;
    public float speed;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetInteger("Nav", nav);
        animator.SetBool("Thow", thow);
        rb.velocity = new Vector2((nav == 1 ? -1 : nav == 2 ? 1 : 0) * speed, rb.velocity.y);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Enemy")
        {
            nav = nav == 1 ? 2 : 1;
        }
        if (collision.gameObject.tag == "Player1")
        {
            nav = 0;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player1")
        {
            nav = nav == 1 ? 2 : 1;
        }
    }
}
