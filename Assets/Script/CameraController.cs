﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    public float offsetSmoothing;
    public EdgeCollider2D ec;
    public MoudelesImage[] prefap;
    public GameObject pos;
    public int maxSoMang;
    public int soMang;

    private GameOverMenu gov;
    private Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position;
        GenerateImg(0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = ((player.transform.position + offset).x > -8.5f ? Vector3.Lerp(transform.position, player.transform.position + offset, offsetSmoothing = Time.deltaTime) : transform.position);
    }

    public void GenerateImg(int up,int upMax)
    {
        soMang += up;
        maxSoMang += upMax;
        if (soMang == 0)
        {
            gov = FindObjectOfType<GameOverMenu>();
            gov.gameOver();
            Time.timeScale = 0f;
            Application.Quit();
        }
        for (int i = 1; i <= maxSoMang; i++)
        {
            Vector3 position = new Vector3(pos.transform.position.x + (i / 2.5f), pos.transform.position.y, pos.transform.position.z);
            Instantiate((soMang >= i * 2 ? prefap[0].prefab : soMang < i * 2 - 1 ? prefap[2].prefab : prefap[1].prefab), position, Quaternion.identity, transform);
        }
    }
}
