﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Rigidbody2D rb;
    public float speed = 2f;
    public float tall;
    public float movementHori = 0f;
    public float movementVert = 0f;
    public Animator animator;
    public float delaydie;
    public Vector3 respawnPoint;

    private bool OnGround = false;
    private float timer = 0.0f;
    private float timedie = 0.0f;
    private CameraController cmr;

    private void Start()
    {
        respawnPoint =  this.transform.position;
        tall = transform.localScale.y * 2f;
        cmr = FindObjectOfType<CameraController>();
    }

    private void Update()
    {
        movementHori = Input.GetAxisRaw("Horizontal");
        movementVert = Input.GetAxisRaw("Vertical");
        animator.SetFloat("Speed", Mathf.Abs(movementHori));
        animator.SetFloat("TimeNotActive", timer);
        animator.SetBool("OnGround", OnGround);
        timedie += Time.deltaTime;
    }

    private void FixedUpdate()
    {
        if (movementHori == 0f)
        {
            timer += Time.deltaTime;
        }
        else
        {
            timer = 0.0f;
            //rb.AddForce(new Vector2(500 * Time.deltaTime * movementHori , 0));
            rb.velocity = new Vector2(movementHori * speed, rb.velocity.y);
            transform.localScale = new Vector2(1 * (movementHori < 0 ? -1 : 1), 1);
        }
        if (movementVert == 0f)
        {
            timer += Time.deltaTime;
        }
        else
        {
            timer = 0.0f;
            if (movementVert > 0f && OnGround)
            {
                OnGround = false;
                rb.velocity = new Vector2(rb.velocity.x, movementVert * tall);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Wall")
        {
            OnGround = true;
        }
        if (collision.gameObject.tag == "MainCamera")
        {
            rb.velocity = new Vector2(0, 0);
        }
        if (collision.gameObject.tag == "Enemy" && timedie > delaydie)
        {
            timedie = 0;
            cmr.GenerateImg(-1, 0);
        }
    }
}
