﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public float respawnDelay;
    public Player gamePlayer;
    public int coins;
    
    public TextMeshProUGUI cointText;

    private void Start()
    {
        gamePlayer = FindObjectOfType<Player>();
        cointText.text = "GOLD: " + coins;
    }
    private void Update()
    {
        
    }

    public void Respawn()
    {
        StartCoroutine("RespawnCoroutine");
    }
    public IEnumerator RespawnCoroutine()
    {
        gamePlayer.gameObject.SetActive(false);
        yield return new WaitForSeconds(respawnDelay);
        gamePlayer.transform.position = gamePlayer.respawnPoint;
        gamePlayer.gameObject.SetActive(true);
    }
    public void addCoins(int numberOfCoins)
    {
        coins += numberOfCoins;
        cointText.text = "GOLD: " + coins;
    }
}
