﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEn_103 : MonoBehaviour
{
    public int loai;
    public bool att;
    public Animator animator;
    public float speed;
    public Rigidbody2D rb;

    private int nav = 1;
    private float timer = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        animator.SetInteger("Loai", loai);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        animator.SetInteger("Nav", nav);
        animator.SetBool("Att", att);
        if (att && timer > 3f)
        {
            att = false;
        }
        rb.velocity = new Vector2((nav==0?0:nav==1?-1:nav==2?1:0) * speed, rb.velocity.y);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Enemy")
        {
            nav = (nav + 1) % 4;      
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player1")
        {
            timer = 0f;
            att = true;
        }
    }
}
