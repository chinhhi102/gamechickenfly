﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectLevelSetting : MonoBehaviour
{
    public void PlayGame(int level)
    {
        SceneManager.LoadScene(level);
    }

    public void BackGame()
    {
        SceneManager.LoadScene(0);
    }
}
