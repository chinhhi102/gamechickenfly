﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScrips : MonoBehaviour
{
    private LevelManager gamelevelManager;
    private CameraController cmr;
    public int valueCoins;
    public int loai;
    private void Start()
    {
        gamelevelManager = FindObjectOfType<LevelManager>();
        cmr = FindObjectOfType<CameraController>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player1")
        {
            if (loai == 1)
            {
                gamelevelManager.addCoins(valueCoins);
            }
            if (loai == 2)
            {
                cmr.GenerateImg(1, 0);
            }
            Destroy(gameObject);
        }
    }
}
