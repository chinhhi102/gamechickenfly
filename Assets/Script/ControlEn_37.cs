﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEn_37 : MonoBehaviour
{
    public int loai;
    public bool hit = false;
    public Animator animator;
    public float speed;
    public Rigidbody2D rb;
    private int nav = 1;

    // Start is called before the first frame update
    void Start()
    {
        animator.SetInteger("Loai", loai);
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("Hit", hit);
        rb.velocity = new Vector2(nav * speed, rb.velocity.y);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Enemy")
        {
            nav *= -1;
        }
        if (collision.gameObject.tag == "Player1")
        {
            nav = 0;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player1")
        {
            nav = -1;
        }
    }
}
