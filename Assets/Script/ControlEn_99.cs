﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEn_99 : MonoBehaviour
{
    public bool nav = false;
    public Animator animator;
    public float speed;
    public Rigidbody2D rb;

    private float x;
    private bool onGround;

    // Start is called before the first frame update
    void Start()
    {
        x = speed;
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("Nav", nav);
        rb.velocity = new Vector2(nav?1:-1 * speed, rb.velocity.y);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Enemy")
        {
            nav = !nav;
        }
        if (collision.gameObject.tag == "Player1")
        {
            speed = 0;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player1")
        {
            speed = x;
        }
    }
}
